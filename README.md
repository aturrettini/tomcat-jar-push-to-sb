# Python script to push files to aws sb

Python script to push and move files onto a sb. can use sudo su - www.

Credit to Carlos Loredo for initial script and pointing out py can do better than rb!

## To run
assuming you have python3 (may or may not work in py2)

clone this repo

In file push-tomcat-session-exporter, see the `file_name`, `jar_path` and update accordingly (I may make these params)

    pip3 install paramiko

Then run with this : 

    python3 push-tomcat-session-exporter.py

if it doesnt work, it will try to give messages. To see more info use debug param.

    python3 push-tomcat-session-exporter.py debug

