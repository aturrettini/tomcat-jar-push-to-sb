import time
import paramiko
from os.path import expanduser
import sys

debug = "debug" in sys.argv
user_path = expanduser("~")
target_sandboxes = "du010snd031 du010snd046 du010snd047 du010snd067 du010snd108 du010snd109".split(" ")
local_file_name = 'shaded-tomcat-session-exporter-1.0.0.jar'
server_file_name = 'tomcat-session-exporter-1.0.0.jar'
local_jar_path = f"{user_path}/apps/tomcat-session-exporter/target/{local_file_name}"
user_name = 'angidev'
key_name = f"{user_path}/.ssh/angidev.key"
local_file_name = f"{user_path}"
target_path = '/usr/local/sm-tomcat/lib'
temp_target_path = f"/tmp/{server_file_name}"

def main() : 
    sandbox = choose_sandbox()
    ssh = connect(sandbox)
    push_file_to_tmp(ssh)
    copy_file_on_server(ssh)
    ssh.close()

def connect(fqdn) : 
    ssh = paramiko.SSHClient()

    try : 
        k = paramiko.RSAKey.from_private_key_file(key_name)
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname=f"{fqdn}", username=f"{user_name}", pkey=k)
    except BaseException as err:
        print("Error connecting : " + str(err))
        exit()

    dbg("Connected to ssh")

    return ssh

def choose_sandbox() : 
    
    print ("Please choose a sandbox by entering a number")
    for i in target_sandboxes:
        print(f"{target_sandboxes.index(i) + 1}) {i}")

    chosen_sandbox = input()

    max_len = len(target_sandboxes)
    
    if not (int(chosen_sandbox) and int(chosen_sandbox) in range(1, max_len +1)) :
        print (f"Please try again with a response between 0 and {max_len}")
        exit()

    chosen_sandbox = target_sandboxes[int(chosen_sandbox) - 1]

    sb_name = f"{chosen_sandbox}.dev.angi.cloud"
    
    dbg("user choose sb : " + sb_name)

    return sb_name

def push_file_to_tmp(ssh) : 
    sftp = ssh.open_sftp() 
    sftp.put(local_jar_path, temp_target_path)

    dbg("finished SFTPing the jar to /tmp")

def send_cmd(channel, cmdstr) : 
    dbg("Sending Command " + cmdstr + "\n")
    result = ""
    channel.send(cmdstr + "\n")

    while not channel.recv_ready() :
        time.sleep(3)
        #result += channel.recv(9999).decode('UTF-8')
        #dbg(result)

    result += channel.recv(9999).decode('UTF-8')

    dbg("Received : " + result + "\n")

    return result;

def copy_file_on_server(ssh) : 

    channel = ssh.invoke_shell()
    out = channel.recv(9999)

    result = send_cmd(channel, "sudo su - www")
 
    if ("password" in result) : 
        print (f"Issue with sending sudo")
        print (result)
        exit()

    cp_cmd = f'cp {temp_target_path} {target_path}\n'
    
    send_cmd(channel, cp_cmd)
    
  
    result = send_cmd(channel, f'ls  {target_path}\n')

    if (server_file_name not in result) : 
        dbg("Did not succeed copying : " + result)

def dbg(str) : 
    if debug : 
        print(str)

main()
